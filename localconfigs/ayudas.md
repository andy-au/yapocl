# Helper Docker

## Images

Build image from Dockerfile

```
docker build -f localconfigs/Dockerfile -t testyapocl .
```

## Stack

Deploy stack from create image in previous step and configured in deploy.yml file

```
docker stack deploy --with-registry-auth -c localconfigs/deploy.yml testyapocl
```

For destroy stack

```
 docker stack rm testyapocl
```

For inspect stack service

```
docker service inspect testyapocl
```

## Secrets

The secrets are mapped to the stack in the _secrets_ base property and then attached to the service.

The secrests inside the _container_ are located inside the _/run/secrets/_ folder
