export NODE_ENV='dev'
export APP_PORT=5000
export ENABLED_STACK='true'
export REDIS_HOST="172.17.0.1"
export REDIS_PORT="6378"
export ALLOWED_ORIGINS="localhost:3000, localhost:18000"