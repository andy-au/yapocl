import app from '../app';
import Debug from 'debug';
import http from 'http';
import displayRoutes from 'express-routemap';

const port = normalizePort(process.env.PORT || 3000);
app.set('port', port);

const createServer = http.createServer(app);

app.listen(port, () => {
  console.log(`\x1b[34m Service listening on port ${port}`);
  displayRoutes(app);
});

createServer.on('error', onError);
createServer.on('listening', onListening);

function normalizePort(val) {
  const port = parseInt(val, 10);
  if (isNaN(port)) {
    return val;
  }
  if (port >= 0) {
    return port;
  }
  return false;
}

function onError(error) {
  if (error.syscall !== 'listen') {
    throw error;
  }

  const bind = typeof port === 'string' ? 'Pipe ' + port : 'Port ' + port;

  switch (error.code) {
    case 'EACCES':
      console.error(bind + ' requires elevated privileges');
      process.exit(1);
    case 'EADDRINUSE':
      console.error(bind + ' is already in use');
      process.exit(1);
    default:
      throw error;
  }
}

const debug = Debug('Service');
function onListening() {
  const addr = createServer.address();
  const bind = typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port;
  debug('Listening on ' + bind);
}

exports.closeServer = function () {
  createServer.close();
};
