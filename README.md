# Yapo.cl Test

this project contemplates a deployment to heroku, and the use of Redis through the page app.redislabs.com

## Run local service

for run local service you need to follow the following command

```
npm install

npm run nodemon
```

additional, you need to install Redis service in your local machine. then you need to configured .env file with your redis host and redis port, actually you dont need to configured a password for that

## Using service

for using you can use postman application and import the postman collection located in ./postman or you can use a curl command

### Calculate pi number

The secrets are mapped to the stack in the _secrets_ base property and then attached to the service.

The secrests inside the _container_ are located inside the _/run/secrets/_ folder#### Local

```
curl --location --request GET 'localhost:3000/api/v1/pi/?random_limit=20'
```

#### Heroku

```
curl --location --request GET 'https://tl-test-yapocl.herokuapp.com/api/v1/pi/?random_limit=20'
```

### Get UF for day

#### Local

```
curl --location --request GET 'localhost:3000/api/v1/uf'
```

#### Heroku

```
curl --location --request GET 'https://tl-test-yapocl.herokuapp.com/api/v1/uf'
```

## Testing

This project contemplates unit tests that are executed in the pipeline

### Postman

```
For use postman, first you need to install postman, later you can import the postman file located in /postman/Yapo.postman_collection.json

in all environment you can view a test for any encrypt (local, docker and heroku deploys)

```

### JMETER

### Calculate pi number

```
rm -rf jmeter/reports/PI && mkdir jmeter/reports/PI && jmeter -n -t jmeter/pi.jmx -l jmeter/reports/PI/log -e -o jmeter/reports/PI/report
```

#### Get UF for day

```
rm -rf jmeter/reports/UF && mkdir jmeter/reports/UF && jmeter -n -t jmeter/uf.jmx -l jmeter/reports/UF/log -e -o jmeter/reports/UF/report
```

## Docker

step for make docker stack

### 1. Create image

for create image you need to run this command

```
npm run docker
```

### 2. Deploy stack

For deploy your image in a new stack, you need to run this command

```
npm run docker:stack
```

### 3. Delete stack

If for any reason you need to delete a stack, run this command

```
npm run docker:delete-stack
```

## API SECURITY

### ¿Qué componentes usarías para securitizar tu API?.

```
- lo primero seria agregar un rate limit para limitar los ataques por fuerza bruta
- Esta API ya contempla algunas configuraciones de seguridad las cuales son:
  - implementacion de helmet
  - validacion de Cors a traves de una white list
- Revision por pipeline de los issues de seguridad a traves de clair analisis
```

### ¿Cómo asegurarías tu API desde el ciclo de vida de desarrollo?

```
- Implementando reglas de los pipelines, en donde no se podra realizar build ni deployar si estas no se cumplen
- Implementacion de husky en el repositorio, en donde no se podra comitear si no se cumplen
- Implementacion de conventional commit para establecer un orden a la hora de comitear
- Automatizacion de procesos en area de QA agregados al pipeline en ambientes previos (Test y QA)
```

## Imaginate un escenario de alto uso de tu API

Considera que el parámetro de tu método de la API tendrá un valor sobre 1000. Es muy
probable que la API demoré un tiempo no menor para responder (y haga un alto consumo
de CPU). Entonces:

### ¿Qué podrías hacer para escalar horizontalmente la API?. Considera agregar nuevos componentes o el uso de un orquestador

```
Utilizaria un stack en docker o k8s, con esto puedo levantar replicas desde una configuracion de stack, esta desarrollo contempla la creacion de un strack en donde se esta seteando solo en 1 replica, pero es posible configurarlas desde el archivo deploy.yml, en caso de produccion que no es posible esta desplegando constantemente utilizaria herramientas como portainer para revisar el estado de los contenedor de forma grafica y poder aumentar la capacidad en unos cuantos clicks.
```

### De acuerdo a tu propuesta, genera los scripts necesarios (YAML para Kubernetes) o como configurarías tu servicio (PAAS)

```
El script se encuentra disponible para docker en este caso, la ruta en donde se encuentra de localconfigs/deploy.yml, ademas de disponibilizar el servicio, se disponibiliza un contenedor con Redis para realizar el cache de data
```

### ¿Qué podrías hacer para escalar horizontalmente de manera automática?. Considera configuración (YAML para Kubernetes) o configuración de servicio PAAS

```
Para k8s se que es posible de forma nativa realizando una configuración, en el caso de docker revisaria estrategias como orbiter quien ya realiza esta configuracion. https://github.com/gianarb/orbiter
```

## Components Diagram

![Alt text](images/diagrams.jpg?raw=true 'Components diagram')
