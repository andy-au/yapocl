import express, { Express, Request, Response } from 'express';
import config from './src/config/default';
import helmet from 'helmet';
import compression from 'compression';
import cookieParser from 'cookie-parser';
import cors from 'cors';
import Logger from './src/helpers/logging';
import * as dotenv from 'dotenv';

dotenv.config();

import { handleNotFound, responseWithError } from './src/middleware';
import router from './src/routers/router';

const app: Express = express();

app.use(helmet());
app.enable('trust proxy');
app.use(express.json());

app.use(
  express.urlencoded({
    extended: false,
  })
);

app.use(cookieParser());
app.use(compression());

// CORS --------------
const { ALLOWED_ORIGINS } = process.env;
const allowedOrigins = ALLOWED_ORIGINS ? ALLOWED_ORIGINS.split(',') : [];

const corsOptionsDelegate = (req, callback) => {
  const corsOptions: any = {
    credentials: true,
    allowedHeaders: config.headers.allowed,
    methods: 'GET,HEAD,POST,OPTIONS',
  };

  if (allowedOrigins.indexOf(req.header('Origin')) !== -1) {
    corsOptions.origin = true;
  } else {
    corsOptions.origin = false;
  }
  callback(null, corsOptions);
};

app.use('*', cors(corsOptionsDelegate));
// -------------------

const BASE_PATH = `${config.resource.baseResource}${config.resource.version}`;

const HEALTH_PATH = `${BASE_PATH}/health`;

app.get(HEALTH_PATH, (req: Request, res: Response) => {
  res.send(config.resource.health);
});

app.use(BASE_PATH, router);

app.use(handleNotFound);
app.use(responseWithError);

function handleFatalError(error: any) {
  Logger.error('app', 'FATAL_ERROR', {
    payload: error,
  });

  setTimeout(() => {
    process.exit(1);
  }, 100);
}

process.on('unhandledRejection', handleFatalError);
process.on('uncaughtException', handleFatalError);

export default app;
