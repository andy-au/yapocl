import { NextFunction, Request, Response } from 'express';
import codeResponse from '../shared/codeResponse';
import Logger from '../helpers/logging';
import { Utils } from '../utils/utils';

const piController = (req: Request, res: Response, next: NextFunction) => {
  const utils = Utils();
  const limit = Number(req.query.random_limit);
  const randomNumber = utils.randomIntFromInterval(limit / 2, limit + 1);

  const substringPi = Math.PI.toString().substring(0, randomNumber + 2);

  const returnData = {
    piCalc: substringPi,
  };

  Logger.info('controller', returnData);
  res.locals.response = codeResponse.OK;
  res.locals.data = returnData;
  next();
};

export default piController;
