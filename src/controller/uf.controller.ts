import { NextFunction, Request, Response } from 'express';
import codeResponse from '../shared/codeResponse';
import Logger from '../helpers/logging';
import buildError from '../shared/buildError';
import { Utils } from '../utils/utils';
import axiosRequest from '../helpers/request';
import Redis from '../helpers/redis';

const MODULE = 'controller';

const ufController = async (
  req: Request,
  res: Response,
  next: NextFunction
) => {
  const util = Utils();
  const redis = await Redis();
  const formatDate = util.getDay();

  let valorUfReturn = await redis.getValue(formatDate);
  if (!valorUfReturn) {
    const requestOption = {
      method: 'GET',
      url: `https://mindicador.cl/api/uf/${formatDate}`,
    };

    try {
      const result = await axiosRequest(requestOption);
      valorUfReturn = result.serie[0].valor;

      redis.setValue(formatDate, valorUfReturn);
      redis.setTtl(formatDate);
    } catch (error: any) {
      const { httpStatus } = error;
      Logger.error(MODULE, 'error in controller', {
        payload: error,
      });
      if (Number(httpStatus) >= 500) {
        return next(buildError(codeResponse.AXIOS_RETURN_ERROR));
      }

      return next(error);
    }
  }

  const dataReturn = {
    valorUf: valorUfReturn,
  };

  Logger.info(MODULE, dataReturn);
  res.locals.response = codeResponse.OK;
  res.locals.data = dataReturn;

  next();
};

export default ufController;
