import testController from '../pi.controller';
import codeResponse from '../../shared/codeResponse';

const nextSpy = jest.fn();
const res: any = {
  locals: {
    response: {},
    data: { piCalc: '' },
  },
};
const randomIntFromIntervalSpy = jest.fn();

jest.mock('../../utils/utils', () => ({
  Utils: () => {
    return {
      randomIntFromInterval: randomIntFromIntervalSpy,
    };
  },
}));

describe('Controller', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  it('should random number its 5', () => {
    randomIntFromIntervalSpy.mockReturnValue(5);
    const request: any = {
      query: {
        random_limit: 5,
      },
    };
    testController(request, res, nextSpy);
    expect(res.locals.response).toBe(codeResponse.OK);
    expect(res.locals.data).toStrictEqual({ piCalc: '3.14159' });
  });
  it('should random number its 10', () => {
    randomIntFromIntervalSpy.mockReturnValue(10);
    const request: any = {
      query: {
        random_limit: 10,
      },
    };
    testController(request, res, nextSpy);
    expect(res.locals.response).toBe(codeResponse.OK);
    expect(res.locals.data).toStrictEqual({ piCalc: '3.1415926535' });
  });
  it('should random number its 9', () => {
    randomIntFromIntervalSpy.mockReturnValue(9);
    const request: any = {
      query: {
        random_limit: 10,
      },
    };
    testController(request, res, nextSpy);
    expect(res.locals.response).toBe(codeResponse.OK);
    expect(res.locals.data).toStrictEqual({ piCalc: '3.141592653' });
  });
  it('should call next correctly', () => {
    randomIntFromIntervalSpy.mockReturnValue(9);
    const request: any = {
      query: {
        random_limit: 5,
      },
    };
    testController(request, res, nextSpy);
    expect(nextSpy).toHaveBeenCalledTimes(1);
  });
});
