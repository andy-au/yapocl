import { AppError } from './AppError';

const buildError = (
  { httpStatus, response, reason, errorMessage },
  details = null
) => {
  const name = response.status;
  const errors = details || [{ reason }];
  const message =
    errorMessage || 'An error occurred while processing your request.';
  return new AppError(name, httpStatus, response.code, message, errors);
};

export default buildError;
