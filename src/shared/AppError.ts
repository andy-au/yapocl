export class AppError extends Error {
  code: number;
  errors: string[];
  httpCode: number;
  isOperational: boolean;
  name: string;
  message: string;

  constructor(
    name: string,
    httpCode: number,
    code: number,
    message: string,
    errors: string[],
    isOperational = true
  ) {
    super();
    this.name = name;
    this.code = code;
    this.httpCode = httpCode;
    this.message = message;
    this.isOperational = isOperational;
    this.errors = errors;
  }
}
