import { StatusCodes } from 'http-status-codes';

const codeResponse = {
  OK: {
    response: {
      code: 0,
      status: 'Success',
    },
    reason: 'The request has succeeded',
    httpStatus: StatusCodes.OK,
  },
  NOT_FOUND: {
    response: {
      code: 404,
      status: 'NOT FOUND',
    },
    reason: 'The server can not find requested resourced',
    errorMessage: 'An error occurred while processing your request',
    httpStatus: StatusCodes.NOT_FOUND,
  },
  INTERNAL_SERVER_ERROR: {
    response: {
      code: 500,
      status: 'SERVER ERROR',
    },
    reason: 'Server error',
    errorMessage: 'An error occurred while processing your request',
    httpStatus: StatusCodes.INTERNAL_SERVER_ERROR,
  },
  AXIOS_RETURN_ERROR: {
    response: {
      code: 1001,
      status: 'SERVER ERROR',
    },
    reason: 'error in axios response',
    httpStatus: StatusCodes.INTERNAL_SERVER_ERROR,
    errorMessage: 'An error occurred in response from axios',
  },
  REDIS_NOT_CONFIGURED: {
    response: {
      code: 1002,
      status: 'INTERNAL SERVER ERROR',
    },
    reason: 'Server Error',
    errorMessage: 'Redis client not configured',
    httpStatus: StatusCodes.INTERNAL_SERVER_ERROR,
  },
};

export default codeResponse;
