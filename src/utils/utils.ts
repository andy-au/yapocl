let date = new Date();
const dayInMillis = 24 * 3600000;

export const Utils = () => {
  const randomIntFromInterval = (min, max) => {
    return Math.floor(Math.random() * (max - min + 1) + min);
  };

  const getDay = () => {
    const day = date.getDate();
    const month = date.getMonth() + 1;
    const year = date.getFullYear();

    return `${day}-0${month}-${year}`;
  };

  const validateChangeDate = (dateNew) => {
    const days1 = Math.floor(date.getTime() / dayInMillis);
    const days2 = Math.floor(dateNew.getTime() / dayInMillis);
    if (days1 < days2) {
      date = dateNew;
      return true;
    }
    return false;
  };

  return {
    randomIntFromInterval,
    getDay,
    validateChangeDate,
  };
};
