/**
 * @param {string} namespace
 * @param {any} object
 */
const info = (namespace: string, object: any) => {
  console.info(`[${getTimeStamp()}] [INFO] [${namespace}]`, object);
};

const error = (namespace: string, message: string, object?: any) => {
  console.error(
    `[${getTimeStamp()}] [ERROR] [${namespace}] ${message}`,
    object
  );
};

const getTimeStamp = (): string => {
  return new Date().toISOString();
};

export default {
  info,
  error,
};
