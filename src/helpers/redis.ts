import Logger from '../helpers/logging';
import * as redis from 'redis';

interface configRedisType {
  url: string;
  password?: string;
}

const { REDIS_HOST, REDIS_PORT, REDIS_PASSWORD } = process.env;
const configRedis: configRedisType = {
  url: `redis://${REDIS_HOST}:${REDIS_PORT}`,
};

if (REDIS_PASSWORD) configRedis.password = REDIS_PASSWORD;
const redisClient = redis.createClient(configRedis);

redisClient.connect();
redisClient.on('error', function (error) {
  console.error(error);
});

const MODULE = 'redis';

const Redis = async () => {
  if (!redisClient) {
    Logger.error('Redis not configured', MODULE, {});
    throw new Error('Redis not configured');
  }

  const getValue = async (value: string) => {
    return await redisClient.get(value);
  };

  const setValue = async (key: string, value: string) => {
    redisClient.set(key, value);
  };

  const setTtl = async (value: string) => {
    const redisTtl = await redisClient.ttl(value);

    if (redisTtl === -1) {
      redisClient.expire(value, 1440 * 60);
    }
  };

  return {
    getValue,
    setValue,
    setTtl,
  };
};

export default Redis;
