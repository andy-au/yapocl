import Logger from '../logging';

jest.mock('../logging', () => ({
  info: jest.fn(),
  error: jest.fn(),
}));

describe('Logging', () => {
  it('should call info', () => {
    Logger.info('mock', {
      test: 'testMock',
    });
    expect(Logger.info).toBeCalledWith('mock', {
      test: 'testMock',
    });
  });

  it('should call error', async () => {
    await Logger.error('mock', 'error mock', {
      test: 'testMock',
    });
    expect(Logger.error).toBeCalledWith('mock', 'error mock', {
      test: 'testMock',
    });
  });
});
