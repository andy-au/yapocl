/* eslint-disable @typescript-eslint/ban-types */
import axios, { AxiosStatic } from 'axios';
import requestAxios from '../request';

interface AxiosMock extends AxiosStatic {
  mockResolvedValue: Function;
  mockRejectedValue: Function;
}

jest.mock('axios');
const mockAxios = axios as AxiosMock;

jest.mock('../../helpers/logging', () => ({
  info: jest.fn(),
  error: jest.fn(),
}));

const requestMock = (method: string, url: string) => {
  const dataRequest = {
    method,
    url,
  };

  return requestAxios(dataRequest);
};

describe('request', () => {
  test('calling request correctly', async () => {
    const payload = { data: { data: 'result' } };
    mockAxios.mockResolvedValue(payload);
    const request = await requestMock('GET', 'www.urlmock.ck');
    await expect(request).toStrictEqual({ data: 'result' });
  });

  test('calling request with error.response', async () => {
    const payload = {
      response: {
        status: 500,
        statusText: 'DataMock',
      },
    };
    mockAxios.mockRejectedValue(payload);
    try {
      await requestMock('GET', 'www.urlmock.ck');
    } catch (e: any) {
      expect(e.httpStatus).toEqual(500);
      expect(e.message).toEqual('DataMock');
    }
  });

  test('calling request with error.response.data', async () => {
    const payload = {
      response: {
        status: 500,
        statusText: 'DataMock',
        data: {
          statusCode: 1012,
          status: 'prueba',
        },
      },
    };
    mockAxios.mockRejectedValue(payload);

    try {
      await requestMock('GET', 'www.urlmock.ck');
    } catch (e: any) {
      expect(e.httpStatus).toEqual(500);
      expect(e.message).toEqual('DataMock');
      expect(e.detail).toEqual('{"statusCode":1012,"status":"prueba"}');
    }
  });

  test('calling request with error response.data.data', async () => {
    const payload = {
      response: {
        status: 500,
        statusText: 'DataMock',
        data: {
          statusCode: 1012,
          status: 'prueba',
          data: {
            ErrorFields: [
              {
                code: 1012,
                reason: 'prueba',
              },
            ],
          },
        },
      },
    };

    mockAxios.mockRejectedValue(payload);

    try {
      await requestMock('GET', 'www.urlmock.ck');
    } catch (e: any) {
      expect(e.httpStatus).toEqual(500);
      expect(e.message).toEqual('DataMock');
      expect(e.detail).toEqual(
        '{"statusCode":1012,"status":"prueba","data":{"ErrorFields":[{"code":1012,"reason":"prueba"}]}}'
      );
    }
  });

  test('calling request with error.request', async () => {
    const payload = {
      errno: -3001,
      message: 'DataMock',
      request: {
        message: 'ErrorMockRequest',
      },
    };
    mockAxios.mockRejectedValue(payload);
    try {
      await requestMock('GET', 'www.urlmock.ck');
    } catch (e: any) {
      expect(e.httpStatus).toEqual(-3001);
      expect(e.message).toEqual('DataMock');
    }
  });

  test('calling LDR and get error not mapped', async () => {
    const payload = {
      message: 'Error en request',
    };
    mockAxios.mockRejectedValue(payload);
    try {
      await requestMock('GET', 'www.urlmock.ck');
    } catch (e: any) {
      expect(e.httpStatus).toEqual(999);
      expect(e.message).toEqual('Error en request');
    }
  });
});
