import Logger from '../helpers/logging';
import axios, { AxiosRequestConfig, AxiosResponse } from 'axios';

const METHOD = 'request';
interface requestType {
  method: string;
  url: string;
}
const axiosRequest = async ({ method, url }: requestType) => {
  const requestOption = {
    method,
    url,
    json: true,
  };

  try {
    const result: AxiosResponse = await axios(
      requestOption as AxiosRequestConfig
    );

    return result.data;
  } catch (error: any) {
    let dataReturn;

    if (error.response) {
      dataReturn = {
        httpStatus: Number(error.response.status),
        message: error.response.statusText,
        detail: JSON.stringify(error.response.data),
      };
    } else if (error.request) {
      dataReturn = {
        httpStatus: error.errno,
        message: error.message,
      };
    } else {
      dataReturn = {
        httpStatus: 999,
        message: error.message,
      };
    }
    Logger.error(METHOD, dataReturn.message, dataReturn);

    throw new RequestError(dataReturn);
  }
};

type RequestErrorType = {
  httpStatus: number;
  message: string;
  detail?: string;
};

class RequestError extends Error {
  httpStatus: number;
  message: string;
  detail?: string;
  constructor(data: RequestErrorType) {
    super();
    this.httpStatus = data.httpStatus;
    this.message = data.message;
    this.detail = data.detail;
  }
}

export default axiosRequest;
