import Logger from '../../helpers/logging';

const nextSpy = jest.fn();
const initSpy = jest.fn();

const reqMock: any = {};
const resMock: any = {};

jest.mock('../../helpers/logging', () => ({
  info: jest.fn(),
  error: jest.fn(),
}));

jest.mock('uuid', () => ({
  v4: () => 'e1991c64-641b-11ec-90d6-0242ac120003',
}));

import InitCallMiddleware from '../initCall';
describe('initCall', () => {
  describe('when not exist errors', () => {
    beforeEach(() => {
      jest.clearAllMocks();
      InitCallMiddleware(reqMock, resMock, nextSpy);
    });

    it('should call logger once', () => {
      expect(Logger.info).toHaveBeenCalledTimes(1);
      expect(Logger.info).toBeCalledWith('initCall', {
        transactionId: 'e1991c64641b11ec',
      });
    });

    it('should call next once', () => {
      expect(nextSpy).toHaveBeenCalledTimes(1);
    });
  });
});
