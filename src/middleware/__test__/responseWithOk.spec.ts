import responseWithOk from '../responseWithOk';
import Logger from '../../helpers/logging';

const sendSpy = jest.fn();
const reqMock: any = {
  transactionId: 'qasd123123',
};

jest.mock('../../helpers/logging', () => ({
  info: jest.fn(),
  error: jest.fn(),
}));

describe('response Ok middleware', () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });
  describe('when not exist errors', () => {
    const resMock: any = {
      locals: {
        data: {},
        response: {
          response: {
            code: 200,
            status: 'Success',
          },
          httpStatus: 200,
        },
        description: 'La información fue encontrada correctamente',
      },

      status: () => {
        return {
          send: sendSpy,
        };
      },
    };

    beforeEach(() => {
      responseWithOk(reqMock, resMock);
    });

    it('should call logger info once', () => {
      expect(Logger.info).toHaveBeenCalledTimes(1);
    });
    it('should call response send method', () => {
      const sendParam = {
        status: 'Success',
        data: {},
        transactionId: 'qasd123123',
      };

      expect(sendSpy).toBeCalledWith(sendParam);
    });
  });

  it('should call logger info once when no have locals.data', () => {
    const resMock: any = {
      locals: {
        response: {
          response: {
            code: 200,
            status: 'Success',
          },
          httpStatus: 200,
        },
        description: 'La información fue encontrada correctamente',
      },

      status: () => {
        return {
          send: sendSpy,
        };
      },
    };
    responseWithOk(reqMock, resMock);
    expect(Logger.info).toHaveBeenCalledTimes(1);
  });

  describe('when throw error', () => {
    it('should data is not object', () => {
      const resMock: any = {
        locals: {
          data: 'Not object',
          response: {
            response: {
              code: 200,
              status: 'Success',
            },
          },
          description: 'La información fue encontrada correctamente',
          httpStatus: 200,
        },
      };

      expect(() => {
        responseWithOk(reqMock, resMock);
      }).toThrow('data must be object');
    });

    it('should response is undefined', () => {
      const resMock: any = {
        locals: {
          data: {},
          description: 'La información fue encontrada correctamente',
          httpStatus: 200,
        },
      };

      expect(() => {
        responseWithOk(reqMock, resMock);
      }).toThrow('response must be defined in res.locals');
    });

    it('should response is not object', () => {
      const resMock: any = {
        locals: {
          data: {},
          response: 'not object',
          description: 'La información fue encontrada correctamente',
          httpStatus: 200,
        },
      };

      expect(() => {
        responseWithOk(reqMock, resMock);
      }).toThrow('response must be data must be object');
    });
  });
});
