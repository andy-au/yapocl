/* eslint-disable @typescript-eslint/no-var-requires */
import buildError from '../../shared/buildError';

import { Logger } from '../../helpers';

import config from '../../config/default';

jest.mock('../../config/default', () => {
  return {
    enabledStack: true,
  };
});

const constructorSpy = jest.fn();
const responseSpy = jest.fn();

const AppErrorMock = class {
  constructor(...args) {
    constructorSpy(...args);
  }
};

jest.mock('../../helpers/logging', () => ({
  info: jest.fn(),
  error: jest.fn(),
}));

jest.mock('../../config/default', () => {
  return {
    enabledStack: true,
  };
});

jest.mock('../../shared/AppError', () => {
  return {
    AppError: AppErrorMock,
  };
});

jest.mock('../../shared/buildError', () => jest.fn());

import responseWithError from '../responseWithError';

describe('response with error middleware', () => {
  const jsonSpy = jest.fn();

  describe('when method is success', () => {
    describe('when error is instance of AppError and enabledStack is true', () => {
      const req: any = {};
      const next: any = {};
      const resMock: any = {
        status: () => {
          return {
            json: jsonSpy,
          };
        },
      };

      const error: any = new AppErrorMock('Error test');

      beforeEach(() => {
        config.enabledStack = true;
        jest.resetAllMocks();
        responseWithError(error, req, resMock, next);
      });

      it('should be an instance of AppError', () => {
        expect(error).toBeInstanceOf(AppErrorMock);
      });

      it('should call logger info once', () => {
        expect(Logger.info).toHaveBeenCalledTimes(1);
      });

      it('should call response send method', () => {
        expect(jsonSpy).toHaveBeenCalledTimes(1);
      });
    });

    describe('when error is instance of AppError and enabledStack is false', () => {
      const req: any = {};
      const next: any = {};
      const resMock: any = {
        status: () => {
          return {
            json: jsonSpy,
          };
        },
      };

      const error: any = new AppErrorMock('Error test');

      beforeEach(() => {
        config.enabledStack = false;
        jest.resetAllMocks();
        responseWithError(error, req, resMock, next);
      });

      it('should be an instance of AppError', () => {
        expect(error).toBeInstanceOf(AppErrorMock);
      });
    });
  });
});
