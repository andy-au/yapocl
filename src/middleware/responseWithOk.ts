import { Request, Response } from 'express';
import Logger from '../helpers/logging';

const MODULE = 'responseWithOk';
interface RequestCustom extends Request {
  transactionId: string;
}

const responseWithOk = (req: RequestCustom, res: Response) => {
  const { data = null, response } = res.locals;

  if (data && typeof data !== 'object') {
    throw TypeError('data must be object');
  }

  if (!response) {
    throw TypeError('response must be defined in res.locals');
  }

  if (typeof response !== 'object') {
    throw TypeError('response must be data must be object');
  }

  const {
    response: { status, code },
    httpStatus,
  } = response;

  Logger.info(MODULE, {
    httpStatus,
    code,
  });

  const transactionId = req.transactionId;
  if (data) {
    return res.status(httpStatus).send({ status, transactionId, data });
  }

  return res.status(httpStatus).send();
};

export default responseWithOk;
