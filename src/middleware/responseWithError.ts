/* eslint-disable @typescript-eslint/no-unused-vars */
import { AppError } from '../shared/AppError';
import config from '../config/default';
import codeResponse from '../shared/codeResponse';
import buildError from '../shared/buildError';
import { Logger } from '../helpers';
import { Request, Response, NextFunction } from 'express';

const MODULE = 'responseWithError';

const getError = (error: any) => {
  if (error instanceof AppError) {
    return error;
  }

  Logger.info(MODULE, {
    payload: error,
  });

  const appError: any = buildError(codeResponse.INTERNAL_SERVER_ERROR);
  appError.stack = error.stack;

  return appError;
};
interface RequestCustom extends Request {
  transactionId: string;
}

interface ResponseType {
  status: string;
  transactionId: string;
  data: {
    errorMsg: string;
    code: number;
    errors: string[];
  };
  stack?: string;
}

const responseWithError = (
  error: TypeError | AppError,
  req: RequestCustom,
  res: Response,
  next: NextFunction
) => {
  const appError = getError(error);
  const responseData: ResponseType = {
    status: appError.name,
    transactionId: req.transactionId,
    data: {
      errorMsg: appError.message,
      code: appError.code,
      errors: appError.errors,
    },
  };

  if (config.enabledStack) {
    responseData.stack = appError.stack;
  }

  Logger.info(MODULE, {
    httpStatus: appError.httpCode,
    code: appError.code,
  });

  res.status(appError.httpCode).json(responseData);
};

export default responseWithError;
