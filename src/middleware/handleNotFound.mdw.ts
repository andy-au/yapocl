import { Request, Response, NextFunction } from 'express';
import codeResponse from '../shared/codeResponse';
import buildError from '../shared/buildError';
import Logger from '../helpers/logging';

const MODULE = 'handlerNotFound';

const handlerNotFound = (req: Request, res: Response, next: NextFunction) => {
  Logger.info(MODULE, { message: `URL ${req.originalUrl} not found` });
  next(buildError(codeResponse.NOT_FOUND));
};

export default handlerNotFound;
