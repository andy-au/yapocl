import { NextFunction, Request, Response } from 'express';
import Logger from '../helpers/logging';
import { v4 } from 'uuid';

const MODULE = 'initCall';
interface RequestCustom extends Request {
  transactionId: string;
}

const initCall = (req: RequestCustom, res: Response, next: NextFunction) => {
  req.transactionId = v4().toString().replace(/-/g, '').substring(0, 16);
  Logger.info(MODULE, {
    transactionId: req.transactionId,
  });

  return next();
};

export default initCall;
