import headers from './headers';

const nameApp = 'Test';

export default {
  nameApp,
  resource: {
    appPort: 3001,
    apiName: nameApp,
    baseResource: '/api',
    version: '/v1',
    domain: '/',
    health: {
      status: 'Up & Running',
      service: nameApp,
    },
  },
  enabledStack: process.env.ENABLED_STACK === 'true' ? true : false,
  'allow-origin': process.env.CONTROL_ALLOW_ORIGIN,
  headers,
};
