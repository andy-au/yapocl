const { ALLOWED_HEADERS } = process.env;
export default {
  allowed: ALLOWED_HEADERS ? ALLOWED_HEADERS.split(',') : [],
};
