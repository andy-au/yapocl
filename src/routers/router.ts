import express from 'express';
import piController from '../controller/pi.controller';
import ufController from '../controller/uf.controller';
import { responseWithOk, initCall } from '../middleware';

const router = express.Router();

router.get('/pi/', [initCall, piController, responseWithOk]);

router.get('/uf', [initCall, ufController, responseWithOk]);

export = router;
